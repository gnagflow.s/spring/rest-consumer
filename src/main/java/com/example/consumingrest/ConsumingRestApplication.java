package com.example.consumingrest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

@SpringBootApplication
public class ConsumingRestApplication {

    private static final Logger log = LoggerFactory.getLogger(ConsumingRestApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(ConsumingRestApplication.class, args);
    }

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }

    @Bean
    public CommandLineRunner run(RestTemplate restTemplate, WebClient.Builder webClientBuilder) {
        final String url = "https://quoters.apps.pcfone.io/api/random";
        return args -> {
            Quote quote = restTemplate.getForObject(url, Quote.class);
            log.info(quote.toString());

            quote = webClientBuilder.build()
                    .get()
                    .uri(url)
                    .retrieve()
                    .bodyToMono(Quote.class)
                    .block();

            log.info(quote.toString());

            ResponseEntity<Quote> response = webClientBuilder.build()
                    .get()
                    .uri(url)
                    .retrieve()
                    .toEntity(Quote.class)
                    .block();

            log.info(response.getBody().toString());
            log.info(response.getHeaders().toString());
        };
    }

    @Bean
    public WebClient.Builder webClientBuilder() {
        return WebClient.builder();
    }
}